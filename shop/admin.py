# -*- coding: utf-8 -*-
from django.contrib import admin


from django.contrib import admin
from .models import Category, Product
class CategoryAdmin(admin.ModelAdmin):
	list_display = ['name', 'slug']
	prepopulated_fields = {'slug': ('name',)}# te ktore sie same wypełnią
class ProductAdmin(admin.ModelAdmin):
	list_display = ['name', 'slug', 'price', 'stock','available', 'created', 'updated']
	list_filter = ['available', 'created', 'updated']
	# lista kolumn, które mogą być edytowane z poziomu listy wyswietlanej na stronei witryny admina
	# każda kolumna z list_Editable musi być wymieniona w list_display !
	list_editable = ['price', 'stock', 'available']
	prepopulated_fields = {'slug': ('name',)}

admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
# Register your models here.
