# -*- coding: utf-8 -*- 
from django.contrib import admin


from django.contrib import admin
from .models import Order, OrderItem

#Model In Line dla modelu OrderItem 
# Wykorzystujemy ModelInline dla modelu OrderItem, 
# aby dołączyć go na miejscu, w klasie OrderAdmin. 
# Taki sposób dołączenia modelu pozwala na jego
# pojawienie się na tej samej stro- nie edycji, tak jak w przypadku modelu nadrzędnego.
class OrderItemInline(admin.TabularInline):
	model = OrderItem
	raw_id_fields = ['product']
class OrderAdmin(admin.ModelAdmin):
	list_display = ['id', 'first_name', 'last_name', 'email','address', 'postal_code', 'city', 'paid','created', 'updated']
	list_filter = ['paid', 'created', 'updated']
	inlines = [OrderItemInline]

admin.site.register(Order, OrderAdmin)

# Register your models here.
